<%-- 
    Document   : EventManager
    Created on : Feb 10, 2016, 5:40:39 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="CSS/regularPage.css">
        <title>JSP Page</title>
    </head>
    <body>
        <%!            HttpSession session;
            String username;
            Cookie cookie;
        %>
        <%
            session = request.getSession();
            username = session.getAttribute("username").toString();
            cookie = new Cookie("username", session.getAttribute("username").toString());
            cookie.setMaxAge(60*60*24*7*30);
            response.addCookie(cookie);
        %>
        <h3>Hello <%=username%>,</h3>
        What Would You Like To Do?<br>
        <form action="manage" method="post">
            <input type="submit" name="add" value="Add Event">
            <input type="submit" name="search" value="Search Event">
        </form>
        <form action="logout" method="post">
            <input type="submit" name="logout" value="Logout">
        </form>
    </body>
</html>
