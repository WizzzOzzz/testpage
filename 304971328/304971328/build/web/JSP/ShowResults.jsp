<%-- 
    Document   : ShowResults
    Created on : Feb 10, 2016, 8:10:32 PM
    Author     : Administrator
--%>

<%@page import="java.sql.Connection"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="Actions.Connector"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%!
        PreparedStatement preparedStatement;
        RequestDispatcher dispatcher;
        ResultSet resultSet;
        %>
        <%
        try {
            Connection connection = Connector.connectToDatabase();
            String sqlQuery = "select * from events";
            if (request.getParameter("day") != null && request.getParameter("hour") == null) {
                sqlQuery += " where event_day=" + request.getParameter("day");
            } else if (request.getParameter("hour") != null && request.getParameter("day") == null) {
                sqlQuery += " where event_hour=" + request.getParameter("hour");
            } else if (request.getParameter("day") != null && request.getParameter("hour") != null) {
                sqlQuery += " where event_hour=" + request.getParameter("hour") + " or event_day=" + request.getParameter("day");
            }
            preparedStatement = connection.prepareStatement(sqlQuery);
            resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                %> Description: <%=resultSet.getString("description")%>
                Day: <%=resultSet.getString("event_day")%>
                Hour: <%=resultSet.getString("event_hour")%><br>
                <%
            }
        } catch (Exception e) {
            dispatcher = request.getRequestDispatcher("JSPErrors/ConnectionError.jsp");
            dispatcher.forward(request, response);
            return;
        }
        %>
    </body>
</html>
