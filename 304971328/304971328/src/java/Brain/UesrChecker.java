/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Brain;

import Actions.Connector;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author wiesori
 */
public class UesrChecker extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher;
        Connection connection = null;
        HttpSession session;
        PreparedStatement preparedStatement;
        try {
            connection = Connector.connectToDatabase();
        } catch (Exception e) {
            dispatcher = request.getRequestDispatcher("JSPErrors/ConnectionError.jsp");
            dispatcher.forward(request, response);
            return;
        }
        session = request.getSession();
        if (session.getAttribute("username") == null || session.getAttribute("password") == null) {
            session.setAttribute("username", request.getParameter("username"));
            session.setAttribute("password", request.getParameter("password"));
        }
        try {
            if (session.getAttribute("username") != null) {
                dispatcher = request.getRequestDispatcher("JSP/EventManager.jsp");
                dispatcher.forward(request, response);
                return;
            }
            preparedStatement = connection.prepareStatement("select * from users where username=?");
            preparedStatement.setString(1, request.getParameter("username"));
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                if (resultSet.getString("password").equals(request.getParameter("password")) || session.getAttribute("password") != null) {
                    dispatcher = request.getRequestDispatcher("JSP/EventManager.jsp");
                    dispatcher.forward(request, response);
                } else {
                    dispatcher = request.getRequestDispatcher("JSPErrors/WrongPassword.jsp");
                    dispatcher.forward(request, response);
                }
            } else {
                dispatcher = request.getRequestDispatcher("JSPErrors/WrongPassword.jsp");
                dispatcher.forward(request, response);
            }
        } catch (Exception e) {
            dispatcher = request.getRequestDispatcher("JSPErrors/WrongPassword.jsp");
            dispatcher.forward(request, response);
        }
    }
}
