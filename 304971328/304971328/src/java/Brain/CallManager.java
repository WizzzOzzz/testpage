/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Brain;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public class CallManager extends HttpServlet {

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }

    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher;
        if (request.getParameter("add")!= null){
            dispatcher = request.getRequestDispatcher("JSP/AddEvent.jsp");
            dispatcher.forward(request, response);
        }else{
            dispatcher = request.getRequestDispatcher("JSP/SearchEvent.jsp");
            dispatcher.forward(request, response);
        }
    }

}
