/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Brain;

import Actions.Connector;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Administrator
 */
public class AddEvent extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher dispatcher;
        Connection connection = null;
        HttpSession session;
        PreparedStatement preparedStatement;
        ResultSet resultSet;
        try {
            connection = Connector.connectToDatabase();
        } catch (Exception e) {
            dispatcher = request.getRequestDispatcher("JSPErrors/ConnectionError.jsp");
            dispatcher.forward(request, response);
            return;
        }
        try {
            preparedStatement = connection.prepareStatement("select * from events where (event_day=? and event_hour=?)");
            preparedStatement.setString(1, request.getParameter("day"));
            preparedStatement.setString(2, request.getParameter("hour"));
            resultSet = preparedStatement.executeQuery();
            if (resultSet.next()){
                dispatcher =request.getRequestDispatcher("JSPErrors/EventExist.jsp");
                dispatcher.forward(request, response);
                return;
            }
            if (Integer.parseInt(request.getParameter("day")) <1 || Integer.parseInt(request.getParameter("day")) >31
                    || Integer.parseInt(request.getParameter("hour")) <0 || Integer.parseInt(request.getParameter("hour")) >23){
                dispatcher = request.getRequestDispatcher("JSPErrors/InvalidArguments.jsp");
                dispatcher.forward(request, response);
                return;
            }
                
            preparedStatement = connection.prepareStatement("insert into events (description, event_day, event_hour)"
                    + "values(?,?,?)");
            preparedStatement.setString(1, request.getParameter("description"));
            preparedStatement.setString(2, request.getParameter("day"));
            preparedStatement.setString(3, request.getParameter("hour"));
            preparedStatement.executeUpdate();
            dispatcher = request.getRequestDispatcher("JSP/EventAdded.jsp");
            dispatcher.forward(request, response);
        } catch (Exception e) {
            
        }
    }
}
