/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Actions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author wiesori
 */
public class Connector {
    private static String urlEvents = "jdbc:derby://localhost:1527/my_db";
    

    public static Connection connectToDatabase() throws ClassNotFoundException, SQLException {
        Class.forName("org.apache.derby.jdbc.ClientDriver");
        return DriverManager.getConnection(urlEvents, "root", "root");
    }
}
